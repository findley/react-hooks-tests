export function throttle(func, threshold = 100, scope = null) {
    let timerId;

    return function () {
        let context = scope || this;
        let args = arguments;
        
        clearTimeout(timerId);

        timerId = setTimeout(() => {
            func.apply(context, args);
        }, threshold)
    }
}