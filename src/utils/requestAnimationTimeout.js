import {caf, raf} from './animationFrame';

export const cancelAnimationTimeout = (frame) => caf(frame.id);

export const requestAnimationTimeout = (
  callback,
  delay,
) => {
  let start;
  // wait for end of processing current event handler, because event handler may be long
  Promise.resolve().then(() => {
    start = Date.now();
  });

  const timeout = () => {
    if (Date.now() - start >= delay) {
      callback.call();
    } else {
      frame.id = raf(timeout);
    }
  };

  const frame = {
    id: raf(timeout),
  };

  return frame;
};