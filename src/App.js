import React, { useState } from 'react';
import BigList from './BigList';
import RecipeListItem from './RecipeListItem';
import VList from './VList';

let data = [];
for (let i = 0; i < 2000; i++) {
  data.push(`Thing ${i+1}`);
}

const App = props => {
  const big = (
    <BigList
      itemHeight={66}
      listItem={item => (
        <RecipeListItem name={item} key={item} />
      )}
      getItems={(offset, limit) => {
        return [data.slice(offset, offset+limit), data.length]
      }}/>
  );

  const items = data.map(item => (
    <RecipeListItem name={item} key={item} />
  ));

  const vlist = (
    <VList
      rowHeight={65}
      list={data}
      getItems={(offset, limit) => {
        console.log(`fetch(${offset}, ${limit})`);
        return new Promise(resolve => {
          setTimeout(() => {
            resolve([data.slice(offset, offset+limit), data.length]);
          }, 100);
        });
      }}
      itemCount={data.length}
      renderRow={(item) => (
        <RecipeListItem name={item} />
      )}
      renderLoadingRow={() => (
        <div>Loading...</div>
      )} />
  );

  return (
      <div>
        {vlist}
      </div>
  );
}

export default App;