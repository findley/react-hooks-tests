import React from 'react';
import {
    ListItem,
    ListItemText,
    ListItemAvatar,
    Avatar,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import FolderIcon from '@material-ui/icons/Folder';

const styles = {
    itemRoot: {
        height: 65
    }
}

const RecipeListItem = props => {
    const {name, classes} = props;
    return (
        <ListItem button divider classes={{root: classes.itemRoot}}>
            <ListItemAvatar>
                <Avatar>
                <FolderIcon />
                </Avatar>
            </ListItemAvatar>
            <ListItemText
                primary={name}
                secondary={'Secondary text'}
            />
        </ListItem>
    );
};

export default withStyles(styles)(RecipeListItem);