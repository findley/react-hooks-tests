import {useState, useEffect} from 'react';
import {throttle} from './utils';

export const useWindowScrollPosition = ({throttleMs = 250, maxWaitMs = 500, el = window} = {}) => {
    const [position, setScroll] = useState({
        x: window.pageXOffset,
        y: window.pageYOffset
    })
    let ticking = false;
    let lastUpdate = Date.now() - 150;

    const handle2 = () => {
        if (ticking) return;
        if (Date.now() - lastUpdate < 150) return;

        window.requestAnimationFrame(() => {
            setScroll({
                x: window.pageXOffset,
                y: window.pageYOffset
            });
            ticking = false;
        });

        ticking = true;
    };
    const handle = throttle(() => {
        setScroll({
            x: window.pageXOffset,
            y: window.pageYOffset
        });
    }, throttleMs, maxWaitMs);

    useEffect(() => {
        window.addEventListener('scroll', handle2);

        return () => {
            window.removeEventListener('scroll', handle2);
        };
    }, [])

    return position;
};