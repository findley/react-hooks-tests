import * as React from 'react';
import {WindowScroller, List, AutoSizer} from 'react-virtualized';

const CHUNK_SIZE = 30;
const STATUS_PENDING = 'pending';
const STATUS_LOADING = 'loading';
const STATUS_LOADED = 'loaded';

export default class WindowScrollerExample extends React.Component {
    _windowScroller;
    chunkStatus = [];
    fetchQueue = [];
    processing = false;

    constructor(props) {
        super(props);

        this.state = {
            cache: {},
            count: CHUNK_SIZE 
        }
    }

  render() {
    const {rowHeight} = this.props;
    const {count} = this.state;

    return (
        <WindowScroller
            ref={this._setRef}
            scrollElement={window}>
            {({height, isScrolling, registerChild, onChildScroll, scrollTop}) => (
            <div>
                <AutoSizer disableHeight>
                {({width}) => (
                    <div ref={registerChild}>
                    <List
                        ref={el => { window.listEl = el; }}
                        autoHeight
                        height={height}
                        isScrolling={isScrolling}
                        onScroll={onChildScroll}
                        rowCount={count}
                        rowHeight={rowHeight}
                        rowRenderer={this._rowRenderer}
                        scrollTop={scrollTop}
                        width={width}
                    />
                    </div>
                )}
                </AutoSizer>
            </div>
            )}
        </WindowScroller>
    );
  }

    _rowRenderer = ({index, key, style}) => {
        const {renderRow, renderLoadingRow} = this.props;
        const {cache} = this.state;
        const row = cache[index];
        let content;


        if (!row) {
            this.bufferedFetchChunk(index);
            content = renderLoadingRow();
        } else {
            content = renderRow(row);
        }

        return (
            <div key={key} style={style}>
                {content}
            </div>
        );
    };

    bufferedFetchChunk(index) {
        const chunkIndex = Math.floor(index / CHUNK_SIZE);
        const status = this.chunkStatus[chunkIndex];
        if (status) {
            return;
        }


        this.fetchQueue.push(chunkIndex);
        this.chunkStatus[chunkIndex] = STATUS_PENDING;
        if (this.fetchQueue.length > 3) {
            const oldChunkIndex = this.fetchQueue.shift();
            this.chunkStatus[oldChunkIndex] = undefined;
        }

        if (!this.processing) {
            this.processQueue();
        }
    }

    processQueue() {
        if (this.fetchQueue.length === 0) {
            this.processing = false;
            return;
        }

        this.processing = true;
        setTimeout(() => {
            const chunkIndex = this.fetchQueue.pop();
            this.fetchChunk(chunkIndex).then(() => {
                this.processQueue();
            });
        }, 100);
    }

    fetchChunk(chunkIndex) {
        return new Promise(resolve => {
            const {getItems} = this.props;

            if (this.chunkStatus[chunkIndex] === STATUS_LOADING) {
                return;
            }
            getItems(chunkIndex * CHUNK_SIZE, CHUNK_SIZE).then(([items, count]) => {
                let newCachePart = {};
                for (let i = 0; i < items.length; i++) {
                    newCachePart[(chunkIndex * CHUNK_SIZE)+i] = items[i];
                }
                this.chunkStatus[chunkIndex] = STATUS_LOADED;
                this.setState(state => ({
                    cache: {...state.cache, ...newCachePart},
                    count: count
                }));
                return resolve();
            });
            this.chunkStatus[chunkIndex] = STATUS_LOADING;
        });
    }

    _setRef = windowScroller => {
        this._windowScroller = windowScroller;
    };
}