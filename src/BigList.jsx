import React, {useState} from 'react';
import {useWindowScrollPosition} from './useWindowScrollPosition';

const Spacer = props => (
    <div style={{height: props.height}}></div>
);

const BigList = props => {
    const {listItem, getItems, itemHeight} = props;
    const position = useWindowScrollPosition({throttleMs: 100, maxWaitMs: 300});
    const [data, setData] = useState([]);

    const scrollPos = position.y;
    const windowHeight = window.innerHeight;

    const renderPadding = windowHeight * 2;
    const renderStart = Math.max(0, scrollPos - renderPadding);
    const renderEnd = renderStart + windowHeight + (renderPadding*2);

    const itemsAbove = Math.floor(renderStart / itemHeight);
    const itemsInView = Math.ceil((renderEnd-renderStart) / itemHeight);
    const [items, totalItemCount] = getItems(itemsAbove, itemsInView);
    const itemsBelow = Math.max(0, totalItemCount - itemsInView - itemsAbove);

    const listItems = items.map(item => listItem(item));
    const aboveSpacerHeight = itemsAbove * itemHeight;
    const belowSpacerHeight = itemsBelow * itemHeight;

    return (
        <div id="big-list">
            <Spacer height={aboveSpacerHeight} />
            {listItems}
            <Spacer height={belowSpacerHeight} />
        </div>
    );
}

export default BigList;